<?php

define('SPACE', 'SPACE');
define('PATH', 'PATH');

function console_log($string)
{
    echo '<script>console.log("' . $string  . '");</script>';
}

function _sort($array) 
{ 
    $_array = $array; 
    sort($_array); 
    return $_array; 
}

function _explode($string, $type = SPACE) 
{
    if ($type == SPACE) {
 //       preg_match_all('~"(?:\\\\.|[^\\\\"])*"|\'(?:\\\\.|[^\\\\\'])*\'|[^\s"\']+~', $string, $matches);
        preg_match_all('~"(?:\\\\.|[^\\\\"])*"|\'(?:\\\\.|[^\\\\\'])*\'|\((?:\\\\.|[^\\\\\)])*\)|[^\s"\'\(\)]+~', $string, $matches);
        return array_map(function($match) { return ($match[0] == '"' && $match[strlen($match) - 1] = '"') || ($match[0] == "'" && $match[strlen($match) - 1] = "'") || ($match[0] == "(" && $match[strlen($match) - 1] = ")") ? substr($match, 1, -1) : $match; }, $matches[0]);
    }
    elseif ($type == PATH) {
        //if (end($matches = preg_split('/[\\[\\]]/', $string)) == '') array_pop($matches);
        return array_filter(preg_split('/[\\[\\]]/', $string), function($part) { return $part != ''; }); //$matches;
    }
}

function br2nl($string) 
{ 
    return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string); 
}

function var_dump_export($what) 
{   
    ob_start(); 
    var_dump($what); 
    return ob_get_clean(); 
}

function _var_export($what) 
{ 
    return var_dump_export($what); 
}

function _htmlspecialchars($str) 
{ 
    return str_replace('}', '&#125;', str_replace('{', '&#123;', htmlspecialchars($str))); 
}

function md5_super($string) 
{ 
    return str_replace('/', '', str_replace('+', '', base64_encode(pack('H*', md5($string))))); 
}

function md5_len($string, $len = 6) 
{ 
    return substr(md5_super($string), 0, $len); 
}

function value(&$data, ...$args) 
{
    $temp = &$data;
    if (is_array($args[0])) {       
        $path = $args[0];
        if (func_num_args() == 3) {
            $set = true;
            $value = $args[1];   
        }     
    } 
    else $path = $args;
    foreach ($path as $key) if (is_object($temp)) $temp = &$temp->$key; elseif (is_array($temp)) $temp = &$temp[$key]; else { $temp = [$key => null]; $temp = &$temp[$key]; }
//    foreach ($path as $key) if (is_object($temp)) $temp = &$temp->$key; else $temp = &$temp[$key]; //else { $temp = [$key => null]; $temp = &$temp[$key]; }
    if ($set) {
        $temp = $value;                
        return $data;    
    }
    else return $temp;                
}                                

function serializeArray_to_assoc($array) 
{
    $result = [];
    foreach ($array as $entry) value($result, _explode($entry['name'], PATH), $entry['value']); 
    return $result;
}

function filter_bool($v) 
{ 
    return filter_var($v, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE); 
}

function is_img($img) 
{ 
    if (!getimagesize($img)) return false; else return true; 
}

function is_date($date) 
{ 
    return is_string($date) && (bool) strtotime($date) && date("Y-m-d", strtotime($date)) == $date; 
}

function is_serialized($value, &$result = null) {
	if (!is_string($value) || $value == '' || is_numeric($value)) return false;
	if ($value === 'b:0;') { $result = false; return true; }
	$length	= strlen($value);
	$end = '';
	switch ($value[0]) {
		case 's': if ($value[$length - 2] !== '"') return false;
		case 'b':
		case 'i':
		case 'd': $end .= ';';
		case 'a':
		case 'O': $end .= '}'; if ($value[1] !== ':') return false;
			switch ($value[2]) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				break;
				default: return false;
			}
		case 'N': $end .= ';'; if ($value[$length - 1] !== $end[0]) return false;
		break;
		default: return false;
	}
	if (($result = @unserialize($value)) === false) { $result = null; return false; }
	return true;
}

function is_assoc($array) 
{ 
    if (!is_array($array)) return false; return (bool) count(array_filter(array_keys($array), 'is_string')); 
}

function is_multi($array) 
{ 
    return (is_array($array) && count($array) != count($array, 1)); 
}

function is_simple($array) 
{ 
	if (is_multi($array)) { $keys = is_array($reset = reset($array)) ? array_keys($reset) : false;
		foreach ($array as $row) { 
			if (@array_keys($row) != $keys || sizeof(@array_filter($row, 'is_array')) > 0) return false;
		}
		return $keys;
	}
	return true;
}

function is_md5($md5 ='') 
{ 
    return is_string($md5) && preg_match('/^[a-f0-9]{32}$/', $md5); 
}

function is_email($email ='') 
{ 
    return filter_var($email, FILTER_VALIDATE_EMAIL) ? true : false; 
}

function is_phone($phone ='') 
{ 
    return strlen(preg_replace("/[^0-9]/", '', $phone)) > 6; 
}

/*
function str_after($string, $substring) { if (($pos = strpos($string, $substring)) === false) return $string; else return(substr($string, $pos + strlen($substring))); }
function str_before($string, $substring) { if (($pos = strpos($string, $substring)) === false) return $string; else return(substr($string, 0, $pos)); } 
function starts_with($haystack, $needle) { $length = strlen($needle); return (substr($haystack, 0, $length) === $needle); }
function ends_with($haystack, $needle) { $length = strlen($needle); if ($length == 0) return true; return (substr($haystack, -$length) === $needle); }
*/

function current_url() 
{
	$is_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on");
	$port = (isset($_SERVER["SERVER_PORT"]) && ((!$is_https && $_SERVER["SERVER_PORT"] != "80") || ($is_https && $_SERVER["SERVER_PORT"] != "443")));
	$port = ($port) ? ':'.$_SERVER["SERVER_PORT"] : '';
	$url = ($is_https ? 'https://' : 'http://').$_SERVER["HTTP_HOST"].$port.$_SERVER["REQUEST_URI"];
	return $url;
}

function _parse_url($current_url, $cut = true) 
{
	$url = parse_url($current_url); 
	$url['full'] = $current_url; 
	if (!isset($url['query'])) $url['query'] = ''; 
	$url['page'] = ''; 
	if (isset($url['path'])) $url['page'] = preg_replace('#/+#', '/', ltrim($url['path'], '/')); 	
	if (isset($url['query']) && $url['query'] !== '') $url['page'] .= "?{$url['query']}"; elseif (ends_with($current_url, '?')) $url['page'] .= "?";
	if (isset($url['query'])) parse_str(str_replace('+', '%2B', $url['query']), $url['get']);
    if ($cut) $url['page'] = reset(explode('?', $url['page']));
	return $url;
}

function mime_extension_type($filename, $mime_types = null) {    

    if (!$mime_types) $mime_types = array(    

        'txt' => 'text/plain',    
        'htm' => 'text/html',    
        'html' => 'text/html',    
        'php' => 'text/html',    
        'css' => 'text/css',    
        'js' => 'application/javascript',    
        'json' => 'application/json',    
        'xml' => 'application/xml',    
        'swf' => 'application/x-shockwave-flash',    
        'flv' => 'video/x-flv',    

        // images    
        'png' => 'image/png',    
        'jpe' => 'image/jpeg',    
        'jpeg' => 'image/jpeg',    
        'jpg' => 'image/jpeg',    
        'gif' => 'image/gif',    
        'bmp' => 'image/bmp',    
        'ico' => 'image/vnd.microsoft.icon',    
        'tiff' => 'image/tiff',    
        'tif' => 'image/tiff',    
        'svg' => 'image/svg+xml',    
        'svgz' => 'image/svg+xml',    

        // archives    
        'zip' => 'application/zip',    
        'rar' => 'application/x-rar-compressed',    
        'exe' => 'application/x-msdownload',    
        'msi' => 'application/x-msdownload',    
        'cab' => 'application/vnd.ms-cab-compressed',    

        // audio/video    
        'mp3' => 'audio/mpeg',    
        'qt' => 'video/quicktime',    
        'mov' => 'video/quicktime',    

        // adobe    
        'pdf' => 'application/pdf',    
        'psd' => 'image/vnd.adobe.photoshop',    
        'ai' => 'application/postscript',    
        'eps' => 'application/postscript',    
        'ps' => 'application/postscript',    

        // ms office    
        'doc' => 'application/msword',    
        'rtf' => 'application/rtf',    
        'xls' => 'application/vnd.ms-excel',    
        'ppt' => 'application/vnd.ms-powerpoint',    

        // open office    
        'odt' => 'application/vnd.oasis.opendocument.text',    
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',    
    );    

    $ext = strtolower(array_pop(explode('.', $filename)));    
    if (array_key_exists($ext, $mime_types)) return $mime_types[$ext];          
}    


function remove_accents($str) 
{
  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
  return str_replace($a, $b, $str);
}

function translit($str) 
{
    $cyr = array('а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п', 'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я', 'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П', 'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я');    
    $lat = array('a','b','v','g','d','e','yo','zh','z','i','j','k','l','m','n','o','p', 'r','s','t','u','f','h','c','ch','sh','sht','a','i','y','e','yu','ya', 'A','B','V','G','D','E','Yo','Zh','Z','I','J','K','L','M','N','O','P', 'R','S','T','U','F','H','C','Ch','Sh','Sht','A','I','Y','e','Yu','Ya');     
    return str_replace($cyr, $lat, remove_accents($str));    
}

function to_url($str = '') 
{ 
    if (is_string($str)) return preg_replace('!\s+!', '-', preg_replace('/[^ \w]+/', ' ', strtolower(trim(iconv("UTF-8", "ISO-8859-1//TRANSLIT", translit(strip_tags($str))))))); 
}

function cut_words($str, $limit = 30) 
{
  return implode('', array_slice(preg_split('/([\s,\.;\?\!]+)/', $str, $limit*2+1, PREG_SPLIT_DELIM_CAPTURE), 0, $limit*2-1));
}

function cut_paragraphs($text, $limit) 
{ 
    return implode(PHP_EOL, array_slice(explode(PHP_EOL, $text), 0, $limit)); 
}

function _encode($string) 
{ 
    return str_replace(array('+', '/', '='), array('-', '_', ''), base64_encode($string)); 
}
function _decode($string) 
{ 
    return base64_decode(str_replace(array('-', '_'), array('+', '/'), $string)); 
}
